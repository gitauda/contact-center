[![License](https://img.shields.io/badge/license-Apache%202-4EB1BA.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

# Cloud call center

## Goal: To build a million-level voice communication platform

## Support function points
* Support websocket sdk access to voice platform
* Support rest interface to access the voice platform
* Support http ivr access


## Flow chart

<p align="center">
    <img  src="https://raw.githubusercontent.com/caoliang1918/callcenter/main/fs-api/src/main/resources/static/fs-api.png" alt="Flow chart">
    <img  src="https://raw.githubusercontent.com/caoliang1918/callcenter/main/fs-api/src/main/resources/static/freeswitch流程.png" alt="Flow chart">
    <img  src="https://raw.githubusercontent.com/caoliang1918/callcenter/main/fs-api/src/main/resources/static/outbound.jpg" alt="Flow Chart">
</p>

